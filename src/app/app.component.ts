import { Component } from '@angular/core';
import { MediatorService } from './mediator.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pictionary';

  constructor(private mediator: MediatorService) {

  }
  public clear(){
    this.mediator.broadcast();
  }
}


