import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediatorService {
  //l'évenement pour effacer
  clearevent: BehaviorSubject<any> ;

  constructor() {
    //initialiser clearevent
    this.clearevent = new BehaviorSubject(false); 
   }

   public broadcast() {
     //méthode appelée par l'app component 
     this.clearevent.next(true); 
   }
}
